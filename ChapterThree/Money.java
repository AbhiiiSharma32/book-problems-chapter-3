package ChapterThree;

public class Money {
    public static void main(String[] args){
        int e = countQuarters(1571);
        System.out.println("You will receive " + e + " Quarters");
    }
    
    public static int countQuarters(int cents){
        int a = cents % 100;
        int b = a % 25;
        int c = a - b;
        int d = c / 25;

        return d;
    }
}
