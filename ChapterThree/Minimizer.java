package ChapterThree;

import java.lang.Math;
public class Minimizer {
    public static void main(String[] args){
        int v = min(32, 99, 57);
        System.out.println("The smallest number is: " + v);
    }
    
    public static int min(int a, int b, int c){
        int g = Math.min(a, b);
        int f = Math.min(g, c);
        return f;
    }
}
